import java.util.*;
import java.io.*;

public class AStackClient {
   public static void main(String[] args)
   {
      int size = 5;
      Scanner input = new Scanner(System.in);
      System.out.print("Input File: ");
      String inputFileName = input.nextLine();
      File reader = new File(inputFileName);

      AStack<Integer> intStack = new AStack<Integer>(size);
      AStack<Float> floatStack = new AStack<Float>(size);
      AStack<String> stringStack = new AStack<String>(size);

      try
      {
         Scanner in = new Scanner(reader);
         
         while(in.hasNext())
         {
            if(in.hasNextInt())
            {
               intStack.push(in.nextInt());
            }
            else if (in.hasNextFloat())
            {
               floatStack.push(in.nextFloat());
            }
            else
            {
               stringStack.push(in.next());
            }
            
         }
         
         System.out.print("String: ");
         while(!stringStack.isEmpty())
         {
            System.out.print(stringStack.pop() + " ");
         }
         
         System.out.print("\nFloats: ");

         while(!floatStack.isEmpty())
         {
            System.out.print(floatStack.pop() + " ");
         }
         
         System.out.print("\nIntegers: " );

         while(!intStack.isEmpty())
         {
            System.out.print(intStack.pop() + " ");
         }
         
      }
      catch(IOException e)
      {
         System.out.println("File doesn't exist");
      }
      
      
   }
}
