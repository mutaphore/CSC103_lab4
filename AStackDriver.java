import java.util.*;

public class AStackDriver {
   
   public static void main(String[] args)
   {
      int size = 5;
      AStack<Integer> stack = new AStack<Integer>(size);
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      int temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- push/add (enter the letter a)");
      System.out.println("- pop/delete (enter the letter d)");
      System.out.println("- check if the list is empty (enter the letter e)");
      System.out.println("- peek (enter the letter p)");
      System.out.println("- quit (enter the letter q)");
     
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);
   
         switch (choice)
         {
            case 'a':
               System.out.println("Please enter value to be added to stack: ");
               if(input.hasNextInt())
               {
                  temp = input.nextInt();
                  input.nextLine();
                  stack.push(temp);
                  System.out.println("Added " + temp);
               }
               else
               {
                  System.out.println("Invalid value!");
                  input.nextLine();
               }
               break;
            case 'd':
               try
               {
                  temp = stack.pop();
                  System.out.println("Removed " + temp);
               }
               catch (AStack.MyException e)
               {
                  System.out.println("Invalid operation: stack is empty!");
               }
               break;
            case 'e':
               if(stack.isEmpty())
                  System.out.println("empty");
               else
                  System.out.println("not empty");
               break;
            case 'p':
               try
               {
                  temp = stack.peek();
                  System.out.println("Peek " + temp);
               }
               catch (AStack.MyException e)
               {
                  System.out.println("Invalid operation: stack is empty!");
               }
            break;
            case 'q':
                  System.out.println("Quitting");
               break;
            default:
               System.out.println("Invalid choice!");
               break;     
         }
      }
            
      while(!stack.isEmpty())
      {
         System.out.print(stack.pop() + " ");
      }
   }
}
